package ir.MVP;

/**
 * Created by Mohammad on 7/17/2018.
 */

public interface IUserView {
    void successGetUser(String message);

    void failureGetUser();
}
