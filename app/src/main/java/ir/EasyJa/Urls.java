package ir.EasyJa;

/**
 * Created by PC4 on 11/29/2017.
 */

public class Urls {


    public static String BASE_URL = "http://backup.vocabeaty.com/api/";
    public static String BASE_URL2 = "http://backup.vocabeaty.com/";
//    public static String BASE_URL = "http://api.easyja.ir/api/";
//    public static String BASE_URL2 = "http://api.easyja.ir/";
//    public static String GET_TOKEN = "http://api.easyja.ir/token";
    public static String GET_TOKEN = "http://backup.vocabeaty.com/token";
    public static String SEND_CODE = "Account/SendCode";
    public static String CHECK_CODE = "Account/CheckCode";
    public static String REGISTER_BY_MOBILE = "Account/RegisterByMobile";
    public static String GET_USER_CREDIT = "Account/GetUserCredit";
    public static String GET_USER_DETAIL = "Account/GetUserDetails";
    public static String GET_NEAR_STATES = "ads/GetNearEstatesBySp";
    public static String INSERT_NEW_ADS = "ads/InsertNewAds";
    public static String UPDATE_ADS = "ads/UpdateAds";
    public static String CHECK_VERIFY = "payverify/checkverify";
    public static String RENT_ADS_SHIFT = "ads/RentAdsShift";
    public static String UNRENT_ADS_SHIFT = "ads/UnRentAdsShift";
    public static String GET_ADS_DETAILS = "ads/GetAdsDetails";
    public static String GET_RENT_LIST = "ads/GetRentList";
    public static String GET_USER_ADS_SHIFT_STATUS = "ads/GetUserAdsShiftStatus";
    public static String GET_ESTATE_TYPES_OPTIONS = "ads/GetEstateTypesOptions";
    public static String GET_USER_ADS = "ads/GetUserAds";
    public static String REGISTER_ACCOUNT="Account/SetAccountNumber";
    public static String PAY = "pay/payment";
    public static String SetUserFavoriteAds = "Ads/SetUserFavoriteAds";
    public static String GetUserFavoriteAds = "Ads/GetUserFavoriteAds";


    public static String GET_PRODUCT_LIST = "GetProductsList";
    public static String أٍGET_MEDITATION = "GetMeditation";
    public static String أٍGET_WEEK_MESSAGE = "GetWeekStory";
    public static String أٍGET_SUCCESS_MESSAGE = "GetSuccessMessage";
    public static String GET_TEST_ALL = "GetTestAll";
    public static String GET_TEST_ITEMS = "GetTestItems";
    public static String RESEND_CODE = "ResendActivationCode";
    public static String GET_TODAY_PHRASE = "GetTodayPhrase";
    public static String GET_PRODUCT_GROUP = "GetProductGroup";
    public static String GET_TARGET_GROUP = "GetTargetGroup";
    public static String GET_MONTH_OMEN = "GetMonthOmen";
    public static String SEND_POLL_ANSWER = "SendPollAnswer";
    public static String GET_POLL = "GetPoll";
    public static String GET_CONVERSATION_LIST = "GetConversationList";
    public static String GET_CONVERSATION = "GetConversation";
    public static String SEND_MESSAGE = "SendMessage";
    public static String GET_NEWS = "GetNews";
    public static String GET_SCORE = "GetScore";
    public static String GET_INFO = "GetContact";
    public static String GET_TARGET = "GetTarget";
    public static String ADD_TARGET = "AddTarget";
    public static String EDIT_TARGET = "EditTarget";
    public static String DELETE_TARGET = "DeleteTarget";
    public static String GET_SCORE_DEFINITION = "GetScoreDefinition";
    public static String GET_ORDER = "GetOrder";
    public static String SEND_ORDER = "SendOrder";
    public static String BUY = "Buy";
    public static String ABOUT_US = "AboutUs";
    public static String REGISTER_SESSION = "RegisterSession";
    public static String REGISTER_CUSTOMER = "RegisterCustomer";
    public static String VERIFY_CUSTOMER = "checkactivationcode";
    public static String GET_PRODUCT = "GetProduct";
    public static String GET_REPRESENTATIVE = "GetRepresentative";
    public static String SEND_REPRESENTATIVE = "SendRepresentativeRequest";
    public static String SEND_TEST_ANSWER = "SendTestAnswer";
    public static String GET_DASHBOARD_INFO = "GetDashboardInfo";
    public static String GET_BUYERS = "GetBuyers";
    public static String GET_PROJECTS = "GetProjects";
    public static String REGISTER_BUYER = "RegisterBuyer";
    public static String ADD_PROJECT = "AddProject";
    public static String GET_PROJECT_DETAIL = "GetProjectDetails";
    public static String SEND_PROJECT_DETAIL = "AddProjectDetail";
    public static String HasNewVersionReleased = "HasNewVersionReleased";
    public static String GET_BUYER_DETAIL = "GetBuyerDetail";

    public Urls() {


    }

}
