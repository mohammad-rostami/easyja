

package ir.EasyJa.Croper;

enum CropDemoPreset {
  RECT,
  CIRCULAR,
  CUSTOMIZED_OVERLAY,
  MIN_MAX_OVERRIDE,
  SCALE_CENTER_INSIDE,
  CUSTOM
}
